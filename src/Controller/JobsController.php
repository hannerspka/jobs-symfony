<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\JobOffers;
use App\Form\JobOffersFormType;
use Symfony\Component\HttpFoundation\Request;

class JobsController extends AbstractController{

   /**
    * @Route("/jobs", name="getJobs")
    */
    public function getAllJobs(EntityManagerInterface $doctrine){
        $repo = $doctrine->getRepository(JobOffers::class);
        $jobs = $repo->findAll();
        return $this->render("jobOffers/jobOffers.html.twig", ["jobs"=>$jobs]);
    }

    /**
     * @Route("/job/{id}", name="getJobOffer")
     */
    public function getJobOffer($id, EntityManagerInterface $doctrine)
    {
        $repo = $doctrine->getRepository(JobOffers::class);
        $jobOffer=$repo->find($id);
        return $this-> render("jobOffers/showJobOffer.html.twig",["job" => $jobOffer]);
    }

    /**
     * @Route("/jobs/create", name="postJobOffer")
     */
    public function postJobOffer(Request $req, EntityManagerInterface $doctrine){
        $form= $this->createForm(JobOffersFormType::class);
        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()){
            $jobOffer = $form->getData();
            $company = $this->getUser();
            $jobOffer->setIdCompany($company);
            $doctrine->persist($jobOffer);
            $doctrine->flush();
            $this->addFlash('success', 'Job offer created correctly');
            return $this->redirectToRoute('getJobs');
        }
        return $this->render('jobOffers/formJobOffer.html.twig', ['jobOffersForm'=>$form->createView()]);
    }
    /**
     * @Route("/updatejoboffer/{id}", name="updateJobOffer")
     */
    public function updateJobOffer(JobOffers $jobOffer, Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(JobOffersFormType::class, $jobOffer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jobOffer = $form->getData();
            $doctrine->persist($jobOffer);
            $doctrine->flush();
            $this->addFlash('success', 'Job offer updated correctly');
            return $this->redirectToRoute('getJobs');
        }
        return $this->render('jobOffers/formJobOffer.html.twig', ['jobOffersForm'=>$form->createView()]);
    }

      /**
     * @Route("/deltejoboffer/{id}", name="deleteJobOffer")
     */
    public function deleteJob(JobOffers $jobOffer, EntityManagerInterface $doctrine)
    {
        $doctrine->remove($jobOffer);
        $doctrine->flush();

        return $this->redirectToRoute("getJobs");
    }


}