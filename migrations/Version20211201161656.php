<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211201161656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE company CHANGE field field VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE job_offers ADD id_company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job_offers ADD CONSTRAINT FK_8A4229A632119A01 FOREIGN KEY (id_company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_8A4229A632119A01 ON job_offers (id_company_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE company CHANGE field field VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE job_offers DROP FOREIGN KEY FK_8A4229A632119A01');
        $this->addSql('DROP INDEX IDX_8A4229A632119A01 ON job_offers');
        $this->addSql('ALTER TABLE job_offers DROP id_company_id');
    }
}
